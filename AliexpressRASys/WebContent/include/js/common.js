window.$myTab = $("#bsplus-tabs");
// 获取工程跟路径
function getRootPath() {
	var curWwwPath = window.document.location.href;
	var pathName = window.document.location.pathname;
	return curWwwPath.substring(0, curWwwPath.indexOf(pathName)) + pathName.substring(0, pathName.substr(1).indexOf('/') + 1);
}
jQuery(document).ready(function() {// 首页菜单
	$("ul.navbar-nav>li.mega-menu-dropdown").mouseover(function() {
		$(this).addClass("open");
	}).mouseout(function() {
		$("ul.navbar-nav>li.open").removeClass("open");
	});
	$(window).click(function(e) {
		var _element = $(e.target);
		switch (_element[0].tagName.toLowerCase()) {
		case "i":
		case "a":
			var _a = _element;
			if (!_a.is("a")) {
				_a = _element.closest("a");
			}
			if (_a.hasClass("show-btn")) {
				_a.hide().next("ul.fixed-boxlist").show(300);
			}
		case "span":
			if (_element.hasClass("view-password")) {
				_element.toggleClass("fa-eye fa-eye-slash");
				var $input = _element.prevAll("input");
				if (_element.hasClass("fa-eye")) {
					$input.attr("type", "text");
				} else {
					$input.attr("type", "password");
				}
			}
		case "li":
			var _li = _element;
			if (!_li.is("li")) {
				_li = _element.closest("li");
			}
			if (_li.hasClass("hide-btn")) {
				_li.closest("ul.fixed-boxlist").hide(300).prev("a.show-btn").show();
			} else if (_li.hasClass("to-top")) {
				$("html,body").animate({
					scrollTop : 0
				}, 300, function() {
					$("#check-bill-grid").find("div.table-head.grid-fixed,div.grid-head>div.grid-paging.grid-fixed,div.grid-body div.table-scroll-box.grid-fixed").removeClass("grid-fixed").css({
						top : "",
						width : ""
					});
				});
			}
			break;
		default:
			break;
		}
	});
	$("#update-password").unbind("click").click(function() {
		bsplus.openModal({
			url : bsplus.path + "/user/updatepassword",
			title : "修改密码",
			iconCls : "fa fa-edit",
			saveBtnText : "确认修改",
			onHandleSuccess : function(e) {
				setTimeout(function() {
					"true" == e.data.code && (window.top.location.href = bsplus.path + "/user/exit");
				}, 2000);
			}
		});
	});
});