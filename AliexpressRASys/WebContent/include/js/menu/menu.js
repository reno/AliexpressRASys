;
var Menu = function() {
	"use strict";
	return {
		rendererAction : function(e) {
			return '<button type="button" onclick="Menu.editTemplate(event,' + e.record.id + ',\'' + e.record.menuName + '\')" class="btn btn-info btn-sm"><span class="fa fa-edit" aria-hidden="true"></span>&nbsp;编辑模板</button>';
		},
		editTemplate : function(e, id, menuName) {
			e.stopPropagation();
			$myTab.addTab({
				code : "edit-template",
				title : "编辑模板—" + menuName,
				url : bsplus.path + "/menu/editTemplate?id=" + id,
				showRefresh : false
			});
		}
	}
}();