<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<c:set var="path" value="${pageContext.request.contextPath }" />
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<form class="form-horizontal" method="post" id="ip-config-form">
	<input type="hidden" name="id" value="${config.id}" />
	<div style="padding: 0 200px;">
		<div class="portlet">
			<div class="portlet-title">
				<div class="title">IP地址白名单配置</div>
			</div>
			<div class="portlet-body">
				<div class="row">
					<div class="col-xs-12">
						<div class="form-group">
							<label class="col-sm-2 control-label">IP地址</label>
							<div class="col-sm-10">
								<textarea rows="5" class="form-control" name="ipContent"
									data-bv-notempty placeholder="请输入IP地址,多个请使用英文逗号隔开">${config.ipContent}</textarea>
							</div>
						</div>
					</div>
				</div>
				<div class="row">
					<div class="col-xs-12">
						<div class="form-group">
							<label class="col-sm-2 control-label">备注</label>
							<div class="col-sm-10">
								<textarea rows="2" class="form-control" name="remark">${config.remark}</textarea>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="portlet-title"
				style="background: none; text-align: center;">
				<button type="submit" class="btn green-haze btn-sm">
					<span class="fa fa-save" aria-hidden="true"></span>&nbsp; 保存
				</button>
			</div>
			<div class="portlet-body"></div>
		</div>
	</div>
</form>
<script type="text/javascript">
	$("#ip-config-form").bootstrapValidator({
		excluded : ':disabled',
		message : 'This value is not valid',
		feedbackIcons : {
			valid : 'fa fa-check vicon',
			invalid : 'fa fa-close vicon',
			validating : 'fa fa-refresh vicon'
		}
	}).on('success.form.bv', function(e) {// 表单验证成功
		e.preventDefault();
		$.ajax({
			url : "${path}/ipconfig/save",
			type : "POST",
			data : $("#ip-config-form").serialize(),
			success : function(data) {
				if ("true" === data.code) {
					bsplus.showToast({
						content : data.msg,
						backColor : "#51A351"
					});
				} else {
					$("#ip-config-form").data('bootstrapValidator').resetForm(true);// 重置验证
					bsplus.showToast({
						content : data.msg,
						backColor : "#C9302C"
					});
				}
			}
		});
	});
</script>