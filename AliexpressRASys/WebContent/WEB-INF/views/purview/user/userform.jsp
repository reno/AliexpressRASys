<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<c:set var="path" value="${pageContext.request.contextPath }" />
<form class="form-horizontal" method="post"
	action="${path}/user/addUser">
	<input name="nid" type="hidden" /><input name="id" type="hidden" />
	<div class="row">
		<div class="col-xs-6">
			<div class="form-group">
				<label for="userName" class="col-sm-4 control-label">用户名称</label>
				<div class="col-sm-8">
					<div class="input-icon">
						<i class="fa fa-asterisk"></i> <input type="text"
							class="form-control" name="userName" maxlength="100"
							placeholder="请输入用户名" data-bv-notempty
							data-bv-notempty-message="请输入用户名">
					</div>
				</div>
			</div>
		</div>
		<div class="col-xs-6">
			<div class="form-group">
				<label for="fullName" class="col-sm-4 control-label">真实姓名</label>
				<div class="col-sm-8">
					<div class="input-icon">
						<i class="fa fa-asterisk"></i> <input type="text"
							class="form-control" name="fullName" maxlength="30"
							placeholder="请输入真实姓名" data-bv-notempty
							data-bv-notempty-message="请输入真实姓名">
					</div>
				</div>
			</div>
		</div>
	</div>
	<c:if test="${param.flag!='edit'}">
		<div class="row">
			<div class="col-xs-6">
				<div class="form-group">
					<label for="password" class="col-sm-4 control-label">用户密码</label>
					<div class="col-sm-8">
						<div class="input-icon">
							<i class="fa fa-asterisk"></i><input type="password"
								class="form-control" name="password" id="password"
								placeholder="请输入用户密码" data-bv-notempty
								data-bv-notempty-message="请输入用户密码" data-bv-identical="true"
								maxlength="50" data-bv-identical-field="confirmPassword"
								data-bv-identical-message="前后密码不一致" /> <span
								class="fa fa-eye-slash view-password"></span>
						</div>
					</div>
				</div>
			</div>
			<div class="col-xs-6">
				<div class="form-group">
					<label for="confirmPassword" class="col-sm-4 control-label">确认密码
					</label>
					<div class="col-sm-8">
						<div class="input-icon">
							<i class="fa fa-asterisk"></i> <input type="password"
								class="form-control" id="confirmPassword" name="confirmPassword"
								placeholder="请再次输入密码" maxlength="50" data-bv-notempty
								data-bv-notempty-message="请再次输入密码" data-bv-identical="true"
								data-bv-identical-field="password"
								data-bv-identical-message="前后密码不一致" /><span
								class="fa fa-eye-slash view-password"></span>
						</div>
					</div>
				</div>
			</div>
		</div>
	</c:if>
	<div class="row">
		<div class="col-xs-6">
			<div class="form-group">
				<label for="roleIds" class="col-sm-4 control-label">角色名称</label>
				<div class="col-sm-8">
					<div class="input-icon">
						<i class="fa fa-asterisk"></i><select name="roleIds"
							multiple="multiple" data-placeholder="请选择角色"
							data-direction="down" data-show-checkall="true"
							data-show-checker="true" data-show-filter="true"
							data-select-all-text="全选" class="bsplus-select" id="roleId"
							data-list-field="list" data-text-field="roleName"
							data-value-field="roleId" data-url="${path}/role/showAllList">
						</select>
					</div>
				</div>
			</div>
		</div>
		<div class="col-xs-6">
			<div class="form-group">
				<label for="roleIds" class="col-sm-4 control-label">所属平台</label>
				<div class="col-sm-8">
					<select name="platforms" multiple="multiple"
						data-placeholder="请选择平台" data-show-checkall="true"
						data-show-checker="true" data-show-filter="true"
						data-select-all-text="全选" onchange="User.selectPlatform(this)"
						class="bsplus-select" data-url="${path}/json/platform.json">
					</select>
				</div>
			</div>
		</div>
	</div>
	<div class="row">
		<div class="col-xs-12">
			<div class="form-group">
				<label for="roleIds" class="col-sm-2 control-label">远程账号</label>
				<div class="col-sm-10">
					<select multiple="multiple" id="user-host-account" name="hostIds"
						data-placeholder="请选择平台" data-show-checkall="true"
						data-show-checker="true" data-show-filter="true"
						data-select-all-text="全选" class="bsplus-select"
						data-value-field="id" data-text-field="connectName"
						data-group-field="platform">
					</select>
				</div>
			</div>
		</div>
	</div>
	<div class="row">
		<div class="col-xs-6">
			<div class="form-group">
				<label for="roleIds" class="col-sm-4 control-label">性别</label>
				<div class="col-sm-8">
					<span> <input type="radio" value="1" checked="checked"
						name="sex" data-after-text="男" /></span> <span
						style="margin-left: 50px;"><input type="radio" value="0"
						data-after-text="女" name="sex" /></span>
				</div>
			</div>
		</div>
		<div class="col-xs-6">
			<div class="form-group">
				<label for="roleIds" class="col-sm-4 control-label">职责</label>
				<div class="col-sm-8">
					<input type="text" class="form-control" name="duty" />
				</div>
			</div>
		</div>
	</div>
	<div class="row">
		<div class="col-xs-6">
			<div class="form-group">
				<label class="col-sm-4 control-label">邮箱</label>
				<div class="col-sm-8">
					<input type="email" class="form-control" name="email"
						data-bv-field="email" maxlength="50"
						data-bv-emailaddress-message="邮箱格式错误">
				</div>
			</div>
		</div>
		<div class="col-xs-6">
			<div class="form-group">
				<label class="col-sm-4 control-label">电话号码</label>
				<div class="col-sm-8">
					<input type="text" class="form-control" name="mobile"
						maxlength="50">
				</div>
			</div>
		</div>
	</div>
	<div class="row">
		<div class="col-xs-12">
			<div class="form-group">
				<label for="description" class="col-sm-2 control-label">描述</label>
				<div class="col-sm-10">
					<textarea class="form-control" rows="2" name="memo"></textarea>
				</div>
			</div>
		</div>
	</div>
</form>
<script>
	User.initForm();
</script>