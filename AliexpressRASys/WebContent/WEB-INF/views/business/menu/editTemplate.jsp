<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="priv" uri="/mytag/privilege"%>
<c:set var="path" value="${pageContext.request.contextPath }" />
<link rel="stylesheet" href="${path}/css/template.css?v=${version}"
	type="text/css">
<div style="padding: 10px 20px;">
	<form class="form-horizontal" method="post" id="template-form"
		onsubmit="Template.submit(event)">
		<input name="menuId" type="hidden" value="${param.id}" id="menuId" />
		<input name="categoryId" type="hidden" id="categoryId" />
		<div class="portlet">
			<div class="portlet-title">
				<div class="title">产品属性</div>
				<span style="position: absolute; right: 50px; color: #ffffff;">注：只有服装会选择尺寸</span>
			</div>
			<div class="portlet-body">
				<div class="attr-row">
					<div class="form-group">
						<label class="attr-title">产品分类：</label>
						<div class="attr-info">
							<div class="category-info">
								<div class="input-icon">
									<i class="fa fa-asterisk"></i> <input class="form-control"
										name="parentName" id="category-name" placeholder="请选择产品分类"
										data-bv-notempty data-bv-notempty-message="产品分类不能为空"
										readonly="readonly" />
								</div>
								<button type="button" class="btn btn-info"
									id="select-category-btn">选择产品分类</button>
							</div>
							<div class="goods-type">
								<label class="goods-title">是否服装：</label>
								<div class="goods-info">
									<input class="bsplus-switch select-sort" data-left-text="是"
										data-right-text="否" data-height="32" type="checkbox"
										name="isClothing" id="is-clothing" checked="checked">
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="attr-row">
					<div class="my-form-group">
						<label class="attr-title">基本属性：</label>
						<div class="attr-info">
							<div class="product-info" id="product-info"></div>
						</div>
					</div>
				</div>
			</div>
			<div class="portlet-title"
				style="text-align: center; background: none; height: 50px;">
				<button type="submit" class="btn green-haze btn-sm">
					<span class="fa fa-save" aria-hidden="true"></span>&nbsp; 保存
				</button>
			</div>
		</div>
	</form>
</div>
<div class="modal bsplus-modal fade" id="category-modal"
	style="overflow: visible; width: 1132px; margin: auto; margin-top: auto; padding-left: 17px;">
	<div class="modal-dialog" role="document"
		style="vertical-align: middle;">
		<div class="modal-content">
			<div class="modal-header">
				<button class="close" aria-label="Close" data-dismiss="modal"
					type="button">
					<span class="fa fa-remove"></span>
				</button>
				<h4 class="modal-title">
					<span class=""></span>&nbsp;选择产品分类
				</h4>
			</div>
			<div class="modal-body"
				style="height: auto; max-height: 657px; padding: 10px;">
				<div class="selected-msg">
					<label>当前选中：</label><span id="msg1"></span> <span id="msg2"></span>
					<span id="msg3"></span> <span id="msg4"></span>
				</div>
				<div class="container-fluid" style="height: auto;">
					<div class="category-box">
						<div class="category-item-box">
							<ul class="category-list" id="category1"></ul>
						</div>
						<div class="category-item-box">
							<ul class="category-list" id="category2" style="display: none;"></ul>
						</div>
						<div class="category-item-box">
							<ul class="category-list" id="category3" style="display: none;"></ul>
						</div>
						<div class="category-item-box">
							<ul class="category-list" id="category4" style="display: none;"></ul>
						</div>
					</div>
				</div>
			</div>
			<div class="modal-footer">
				<button class="btn btn-default btn-sm" type="button"
					data-dismiss="modal">
					<span class="fa fa-close" aria-hidden="true"></span>&nbsp;关闭
				</button>
				<button class="btn btn-primary btn-sm" type="button"
					onclick="Template.selectCategory()">
					<span class="fa fa-check" aria-hidden="true"></span>&nbsp;选择
				</button>
			</div>
		</div>
	</div>
</div>
<script src="${path}/js/menu/template.js?v=${version}"
	type="text/javascript" charset="UTF-8"></script>