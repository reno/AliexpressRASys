<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="priv" uri="/mytag/privilege"%>
<c:set var="path" value="${pageContext.request.contextPath }" />
<div class="bsplus-grid" data-grid-type="treegrid" data-id-field="id"
	data-parent-field="id" data-child-field="parentId"
	data-tree-column="menuName" data-url="${path}/menu/showList"
	data-show-pager="false" data-expand-tree-onload="true"
	data-multi-select="false" data-scroll-height="400px"
	data-empty-text="暂时没有相关数据" data-ondataload="onDataload"
	id="select-menu-list">
	<input type="hidden" value="${param.parentId}" id="parentId">
	<div class="grid-body">
		<div class="bsplus-table table-bordered">
			<div class="table-fields">
				<div data-type="select" data-common-cls="text-center"
					data-width="8%">选择</div>
				<div data-field="menuName" data-width="30%">菜单名称</div>
				<div data-field="remark" data-common-cls="text-center"
					data-width="25%">备注</div>
			</div>
		</div>
	</div>
</div>
<script type="text/javascript">
	function onDataload(e) {
		var parentId = $("#parentId").val();
		if (parentId) {
			$("#select-menu-list").select({
				field : "id",
				fieldValues : [ parentId ]
			});
		}
	}
</script>