<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="priv" uri="/mytag/privilege"%>
<c:set var="path" value="${pageContext.request.contextPath }" />
<div class="bsplus-grid" data-grid-type="treegrid" data-id-field="id"
	data-parent-field="id" data-child-field="parentId"
	data-tree-column="menuName" data-url="${path}/menu/showList"
	data-show-pager="false" data-expand-tree-onload="true"
	data-multi-select="true" data-scroll-height="400px"
	data-empty-text="暂时没有相关数据" data-allow-wrap="true" id="menu-list">
	<div class="grid-head">
		<div class="grid-functions">
			<div class="form-group">
				<priv:priv-btn btnname="add">
					<button type="button" class="btn green-haze btn-sm"
						data-action="add"
						data-options="{url:'${path}/menu/menuform',title:'添加模块',width:'700px'}">
						<span class="fa fa-plus" aria-hidden="true"></span>&nbsp;添加
					</button>
				</priv:priv-btn>
				<priv:priv-btn btnname="edit">
					<button type="button" class="btn btn-info btn-sm"
						data-action="edit"
						data-options="{url:'${path}/menu/menuform',title:'编辑模块',width:'700px',dataUrl:'${path}/menu/showUpdate'}">
						<span class="fa fa-edit" aria-hidden="true"></span>&nbsp; 编辑
					</button>
				</priv:priv-btn>
				<priv:priv-btn btnname="delete">
					<button type="button" class="btn btn-danger btn-sm"
						data-action="delete"
						data-options="{url:'${path}/menu/deleteMenu'}">
						<span class="fa fa-remove" aria-hidden="true"></span>&nbsp; 删除
					</button>
				</priv:priv-btn>
			</div>
		</div>
	</div>
	<div class="grid-body">
		<div class="bsplus-table table-bordered">
			<div class="table-fields">
				<div data-type="index" data-common-cls="text-center" data-width="8%">序号</div>
				<div data-type="select" data-common-cls="text-center"
					data-width="8%">选择</div>
				<div data-field="menuName" data-width="30%">菜单名称</div>
				<div data-field="isClothing" data-common-cls="text-center"
					data-width="10%" data-default="未知"
					data-render-map="{false:'否',true:'是'}">是否服装</div>
				<div data-field="categoryName" data-common-cls="text-center"
					data-width="40%">所属目录</div>
				<div data-field="remark" data-common-cls="text-center"
					data-width="20%">备注</div>
				<div data-field="" data-common-cls="text-center"
					data-renderer="Menu.rendererAction" data-width="10%">操作</div>
			</div>
		</div>
	</div>
</div>
<script src="${path}/js/menu/menu.js" type="text/javascript"
	charset="UTF-8"></script>