<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<c:set var="path" value="${pageContext.request.contextPath }" />
<form class="form-horizontal" method="post"
	action="${path}/menu/addMenu">
	<input name="id" type="hidden" />
	<div class="modal-body">
		<div class="form-group">
			<label class="col-sm-2 control-label">菜单名称</label>
			<div class="col-sm-10">
				<div class="input-icon">
					<i class="fa fa-asterisk"></i> <input type="text"
						class="form-control" name="menuName" maxlength="100"
						placeholder="请输入菜单名称" data-bv-notempty
						data-bv-notempty-message="请输入菜单名称" />
				</div>
			</div>
		</div>
		<div class="form-group">
			<label class="col-sm-2 control-label">父级菜单</label>
			<div class="col-sm-10">
				<input type="hidden" class="form-control" name="parentId"
					id="parentId" />
				<div class="row">
					<div class="col-xs-9">
						<input class="form-control" name="parentName" id="parentName"
							placeholder="请选择父级菜单" readonly="readonly" />
					</div>
					<div class="col-xs-3">
						<button type="button" class="btn btn-info" id="select-btn">选择父级菜单</button>
					</div>
				</div>
			</div>
		</div>
		<div class="form-group">
			<label class="col-sm-2 control-label">描述</label>
			<div class="col-sm-10">
				<textarea class="form-control" rows="1" name="remark"></textarea>
			</div>
		</div>
	</div>
</form>
<script type="text/javascript">
	$("#select-btn").click(function() {
		var $btn = $(this);
		$btn.prop("disabled", true);
		bsplus.openModal({// 弹出选择功能界面
			url : bsplus.path + "/menu/menuselect?parentId=" + $("#parentId").val(),
			title : "选择父级菜单",
			dialogCls : "modal-dialog modal-lg",
			saveBtnText : "确定",
			width : "500px",
			onSaveBtnclick : function(opts) {
				var selecteds = $("#select-menu-list").getSelected();
				if (selecteds && selecteds.length > 0) {
					$("#parentId").val(selecteds[0].id);
					$("#parentName").val(selecteds[0].menuName);
				} else {
					$("#parentId").val("");
					$("#parentName").val("");
				}
				bsplus.destroyModal(opts);// 销毁模态框
			},
			ondestroy : function(opts) {
				$btn.prop("disabled", false);
			}
		});
	});
</script>