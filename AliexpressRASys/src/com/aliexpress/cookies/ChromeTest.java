package com.aliexpress.cookies;

import java.io.IOException;
import java.util.Map;

import org.jsoup.Connection;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;

import com.aliexpress.cookies.CookiesParse.ChromeBrowser;

public class ChromeTest {
	private static final String userAgent = "Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/60.0.3112.90 Safari/537.36";

	public static void main(String[] args) throws IOException {
		ChromeBrowser chrome = new ChromeBrowser();
		//String savePath = "D:/java/files/cookies/Cookies";
		//chrome.saveCookiesToFile(savePath, "dianxiaomi.com");// 先在本地把cookies存到文件

		String readPath = "D:/java/files/cookies/Cookies";
		Map<String, String> cookiesOfDianxiaomi = chrome.getCookiesFromFile(readPath);// 把cookies从存储的文件中读取出来
		System.out.println(cookiesOfDianxiaomi);
		Connection.Response res = null;
		Document doc = null;
		String url = "https://www.dianxiaomi.com/sys/index.htm?go=m401";
		Connection connection = Jsoup.connect(url);
		connection.timeout(1000 * 20);
		connection.ignoreContentType(true);
		connection.maxBodySize(0);// 不限长度
		connection.header("User-Agent", userAgent);
		connection.cookies(cookiesOfDianxiaomi);// 把读取出来的cookies加载到本次请求中
		res = connection.method(Connection.Method.GET).execute();
		doc = Jsoup.parse(res.body());
		System.out.println(doc.html());
	}
}
