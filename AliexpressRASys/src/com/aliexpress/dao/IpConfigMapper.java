package com.aliexpress.dao;

import com.aliexpress.model.IpConfig;
import com.aliexpress.model.IpConfigExample;
import java.util.List;
import org.apache.ibatis.annotations.Param;

public interface IpConfigMapper {
    int countByExample(IpConfigExample example);

    int deleteByExample(IpConfigExample example);

    int deleteByPrimaryKey(Byte id);

    int insert(IpConfig record);

    int insertSelective(IpConfig record);

    List<IpConfig> selectByExampleWithBLOBs(IpConfigExample example);

    List<IpConfig> selectByExample(IpConfigExample example);

    IpConfig selectByPrimaryKey(Byte id);

    int updateByExampleSelective(@Param("record") IpConfig record, @Param("example") IpConfigExample example);

    int updateByExampleWithBLOBs(@Param("record") IpConfig record, @Param("example") IpConfigExample example);

    int updateByExample(@Param("record") IpConfig record, @Param("example") IpConfigExample example);

    int updateByPrimaryKeySelective(IpConfig record);

    int updateByPrimaryKeyWithBLOBs(IpConfig record);

    int updateByPrimaryKey(IpConfig record);
}