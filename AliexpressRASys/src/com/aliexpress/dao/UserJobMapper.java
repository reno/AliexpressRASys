package com.aliexpress.dao;

import com.aliexpress.model.UserJob;
import com.aliexpress.model.UserJobExample;
import java.util.List;
import org.apache.ibatis.annotations.Param;

public interface UserJobMapper {
    int countByExample(UserJobExample example);

    int deleteByExample(UserJobExample example);

    int deleteByPrimaryKey(String jobNumber);

    int insert(UserJob record);

    int insertSelective(UserJob record);

    List<UserJob> selectByExample(UserJobExample example);

    UserJob selectByPrimaryKey(String jobNumber);

    int updateByExampleSelective(@Param("record") UserJob record, @Param("example") UserJobExample example);

    int updateByExample(@Param("record") UserJob record, @Param("example") UserJobExample example);

    int updateByPrimaryKeySelective(UserJob record);

    int updateByPrimaryKey(UserJob record);
}