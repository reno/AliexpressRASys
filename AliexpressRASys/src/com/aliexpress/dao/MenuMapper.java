package com.aliexpress.dao;

import java.util.List;

import org.apache.ibatis.annotations.Param;

import com.aliexpress.bean.ProductInfo;
import com.aliexpress.model.Menu;
import com.aliexpress.model.MenuExample;

public interface MenuMapper {
    int countByExample(MenuExample example);

    int deleteByExample(MenuExample example);

    int deleteByPrimaryKey(Integer id);

    int insert(Menu record);

    int insertSelective(Menu record);

    List<Menu> selectByExample(MenuExample example);

    Menu selectByPrimaryKey(Integer id);

    int updateByExampleSelective(@Param("record") Menu record, @Param("example") MenuExample example);

    int updateByExample(@Param("record") Menu record, @Param("example") MenuExample example);

    int updateByPrimaryKeySelective(Menu record);

    int updateByPrimaryKey(Menu record);
	
	Menu selectById(@Param("menuId") String menuId);

	List<ProductInfo> selectProductInfo(@Param("cid") String cid);
}