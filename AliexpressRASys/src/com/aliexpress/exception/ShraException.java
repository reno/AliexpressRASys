package com.aliexpress.exception;

/**
 * @author JIAGUI
 * @email 1257896208@qq.com
 * @date 2017年3月24日 上午10:17:24
 * @description 自定义异常
 */
public class ShraException extends Exception {
	private static final long serialVersionUID = 5719597552022873366L;

	public ShraException() {
		super();
	}

	public ShraException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
		super();
	}

	public ShraException(String message, Throwable cause) {
		super(message, cause);
	}

	public ShraException(String message) {
		super(message);
	}

	public ShraException(Throwable cause) {
		super(cause);
	}
}
