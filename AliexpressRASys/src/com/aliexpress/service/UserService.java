package com.aliexpress.service;

import com.ra.model.RaUser;

public interface UserService {

	int update(RaUser user, String roleIds);

	String selectRoleIdsByKey(Long nid);

	String selectRoleNamesByKey(Long nid);

}
