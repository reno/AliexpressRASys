package com.aliexpress.service;

import java.util.List;

import com.aliexpress.model.IpConfig;

public interface IpConfigService {

	int add(IpConfig config);

	int update(IpConfig config);

	List<IpConfig> selectAllList();

}
