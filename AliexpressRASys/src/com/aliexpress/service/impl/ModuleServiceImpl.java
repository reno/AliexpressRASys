package com.aliexpress.service.impl;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import com.aliexpress.constant.SysConstants;
import com.aliexpress.dao.ModuleFunctionMapper;
import com.aliexpress.dao.ModuleMapper;
import com.aliexpress.dao.RoleModuleMapper;
import com.aliexpress.model.Function;
import com.aliexpress.model.Module;
import com.aliexpress.model.ModuleFunction;
import com.aliexpress.model.ModuleFunctionExample;
import com.aliexpress.model.RoleModule;
import com.aliexpress.model.RoleModuleExample;
import com.aliexpress.service.ModuleService;
import com.ra.model.RaUser;

@Service(value = "moduleServiceImpl")
public class ModuleServiceImpl implements ModuleService {
	@Autowired
	@Qualifier(value = "moduleMapper")
	private ModuleMapper moduleMapper;
	@Autowired
	@Qualifier(value = "roleModuleMapper")
	private RoleModuleMapper roleModuleMapper;
	@Autowired
	@Qualifier(value = "moduleFunctionMapper")
	private ModuleFunctionMapper moduleFunctionMapper;

	public List<Module> selectAllList() {
		return moduleMapper.selectByExample(null);
	}

	public int addModule(Module module) {
		int result = moduleMapper.insertSelective(module);
		if (StringUtils.hasText(module.getFunctionIds())) {
			String[] ids = module.getFunctionIds().split(",");
			List<ModuleFunction> list = new ArrayList<ModuleFunction>();
			ModuleFunction rf = null;
			Integer moduleId = module.getModuleId();
			for (String id : ids) {
				rf = new ModuleFunction();
				rf.setModuleId(moduleId);
				rf.setFunctionId(Integer.valueOf(id));
				list.add(rf);
			}
			result += moduleFunctionMapper.insertList(list);
		}
		return result;
	}

	public int updateModule(Module module) {
		int result = 0;
		if (module.getLevel() != null && module.getLevel() == 0) {// 修改其子节点的启用状态
			result = moduleMapper.updateByPrimaryKeySelective(module) + moduleMapper.updateChildren(module);
		} else {
			result = moduleMapper.updateByPrimaryKeySelective(module);
		}
		if (StringUtils.hasText(module.getFunctionIds())) {
			String[] ids = module.getFunctionIds().split(",");
			List<ModuleFunction> list = new ArrayList<ModuleFunction>();
			ModuleFunction rf = null;
			Integer moduleId = module.getModuleId();
			for (String id : ids) {
				rf = new ModuleFunction();
				rf.setModuleId(moduleId);
				rf.setFunctionId(Integer.valueOf(id));
				list.add(rf);
			}
			ModuleFunctionExample example = new ModuleFunctionExample();
			example.createCriteria().andModuleIdEqualTo(module.getModuleId());
			result += moduleFunctionMapper.deleteByExample(example);
			result += moduleFunctionMapper.insertList(list);
		}
		return result;
	}

	public Module selectByKey(Integer sourceId) {
		return moduleMapper.selectByPrimaryKey(sourceId);
	}

	public int deleteModuleByKey(List<?> list) {
		moduleFunctionMapper.deleteModuleFunctionByKey(list);
		return moduleMapper.deleteModuleByKey(list);
	}

	public List<Module> selectModuleByRoleid(List<?> ids) {
		return moduleMapper.selectModuleByRoleid(ids);
	}

	public List<RoleModule> selectRoleModuleByRoleId(Long roleId) {
		RoleModuleExample example = new RoleModuleExample();
		example.createCriteria().andRoleIdEqualTo(roleId);
		return roleModuleMapper.selectByExample(example);
	}

	public List<Module> selectModuleByUser(RaUser user) {
		return moduleMapper.selectModuleByUser(user);
	}

	public Map<Integer, RoleModule> selectRoleModuleByUser(RaUser user) {
		if (user.getUserName().equals(SysConstants.SUPERMAN)) {// 超级用户不查询
			return null;
		}
		Map<Integer, RoleModule> roleResMap = null;
		List<RoleModule> list = roleModuleMapper.selectRoleModuleByRole(user.getNid());
		if (list != null) {
			roleResMap = new HashMap<Integer, RoleModule>();
			List<Function> flist = null;
			RoleModule crm = null;
			for (RoleModule rs : list) {
				if (roleResMap.containsKey(rs.getModuleId())) {
					crm = roleResMap.get(rs.getModuleId());
					flist = crm.getFlist();
					if (rs.getFunction() != null) {
						if (flist != null) {
							flist.add(rs.getFunction());
						} else {
							flist = new ArrayList<Function>();
							flist.add(rs.getFunction());
						}
					}
					crm.setFlist(flist);
				} else {
					crm = rs;
					flist = new ArrayList<Function>();
					flist.add(rs.getFunction());
					crm.setFlist(flist);
					roleResMap.put(rs.getModuleId(), crm);
				}
			}
		}
		return roleResMap;
	}

	@Override
	public List<Module> selectModuleList(Integer roleId) {
		return moduleMapper.selectModuleList(roleId);
	}

	@Override
	public List<String> selectTestList() {
		return moduleMapper.selectTestList();
	}
}
