package com.aliexpress.service.impl;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.aliexpress.dao.UserRoleMapper;
import com.aliexpress.model.UserRole;
import com.aliexpress.service.UserService;
import com.ra.model.RaUser;

/**
 * @project House-Intro-Web-Service
 * @package com.xinhai.houseintro.web.service.impl
 * @class UserServiceImpl.java
 * @author jiagui E-mail:1257896208@qq.com
 * @date 2015年10月25日 下午5:27:52
 * @description 用户模块service层实现类
 */
@Service(value = "userServiceImpl")
public class UserServiceImpl implements UserService {
	@Autowired
	private UserRoleMapper userRoleMapper;

	@Override
	public int update(RaUser user, String roleIds) {
		int n = 0;
		userRoleMapper.deleteByUserId(user.getNid());
		if (StringUtils.isNotBlank(roleIds)) {
			String[] rids = roleIds.split(",");
			List<UserRole> list = new ArrayList<UserRole>();
			UserRole ur = null;
			for (String rid : rids) {
				ur = new UserRole();
				ur.setRoleId(Long.valueOf(rid));
				ur.setUserId(user.getNid());
				list.add(ur);
			}
			n += userRoleMapper.insertList(list);
		}
		return n;
	}

	@Override
	public String selectRoleIdsByKey(Long nid) {
		return userRoleMapper.selectRoleIdsByKey(nid);
	}

	@Override
	public String selectRoleNamesByKey(Long nid) {
		return userRoleMapper.selectRoleNamesByKey(nid);
	}
}
