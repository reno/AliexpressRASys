package com.aliexpress.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.aliexpress.bean.ProductInfo;
import com.aliexpress.dao.MenuMapper;
import com.aliexpress.dao.TemplateMapper;
import com.aliexpress.model.Menu;
import com.aliexpress.model.MenuExample;
import com.aliexpress.model.Template;
import com.aliexpress.model.TemplateExample;
import com.aliexpress.service.MenuService;

@Service
public class MenuServiceImpl implements MenuService {
	@Autowired
	private MenuMapper mapper;
	@Autowired
	private TemplateMapper templateMapper;

	@Override
	public List<Menu> selectAllList() {
		return mapper.selectByExample(null);
	}

	@Override
	public int deleteMenuByKeys(List<Integer> keyes) {
		MenuExample example = new MenuExample();
		example.createCriteria().andIdIn(keyes);
		return mapper.deleteByExample(example);
	}

	@Override
	public Menu selectByKey(Integer id) {
		return mapper.selectByPrimaryKey(id);
	}

	@Override
	public int addMenu(Menu menu) {
		return mapper.insert(menu);
	}

	@Override
	public int updateMenu(Menu menu) {
		return mapper.updateByPrimaryKeySelective(menu);
	}

	@Override
	public int saveTemplate(Menu menu) {
		List<Template> templates = menu.getTemplates();
		if (null != templates && templates.size() > 0) {
			TemplateExample example = new TemplateExample();
			example.createCriteria().andMenuIdEqualTo(menu.getId());
			templateMapper.deleteByExample(example);
			for (Template template : templates) {
				templateMapper.insert(template);
			}
		}
		return mapper.updateByPrimaryKeySelective(menu);
	}

	@Override
	public Menu selectById(String menuId) {
		return mapper.selectById(menuId);
	}

	@Override
	public List<ProductInfo> selectProductInfo(String cid) {
		return mapper.selectProductInfo(cid);
	}
}
