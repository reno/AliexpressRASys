package com.aliexpress.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.aliexpress.dao.IpConfigMapper;
import com.aliexpress.model.IpConfig;
import com.aliexpress.service.IpConfigService;

@Service(value = "ipConfigServiceImpl")
public class IpConfigServiceImpl implements IpConfigService {
	@Autowired
	private IpConfigMapper ipConfigMapper;

	@Override
	public int add(IpConfig config) {
		return ipConfigMapper.insertSelective(config);
	}

	@Override
	public int update(IpConfig config) {
		return ipConfigMapper.updateByPrimaryKeyWithBLOBs(config);
	}

	@Override
	public List<IpConfig> selectAllList() {
		return ipConfigMapper.selectByExampleWithBLOBs(null);
	}

}
