package com.aliexpress.service;

import java.util.List;

import com.aliexpress.bean.ProductInfo;
import com.aliexpress.model.Menu;


public interface MenuService {

	List<Menu> selectAllList();

	int deleteMenuByKeys(List<Integer> keyes);

	Menu selectByKey(Integer id);

	int addMenu(Menu menu);

	int updateMenu(Menu menu);

	int saveTemplate(Menu menu);

	Menu selectById(String menuId);

	List<ProductInfo> selectProductInfo(String cid);
	
}
