package com.aliexpress.service;

import java.util.List;

import com.aliexpress.bean.PageInfo;
import com.aliexpress.model.Role;
import com.aliexpress.model.RoleModule;

public interface RoleService {
	int selectTotalRecord(Role role);

	List<Role> selectList(Role role, PageInfo pageVo);

	int addRole(Role role);

	int updateRole(Role role);

	int saveRoles(List<Role> list);

	Role selectByKey(Long roleId);

	int deleteRoleByKey(List<?> list);

	List<Role> showAllList();

	int saveRoleModule(List<RoleModule> list);

	List<Role> selectAllUserAndRole(Integer companyId);

	List<Role> selectAll();
}
