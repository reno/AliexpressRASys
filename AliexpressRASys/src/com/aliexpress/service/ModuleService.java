package com.aliexpress.service;

import java.util.List;
import java.util.Map;

import com.aliexpress.model.Module;
import com.aliexpress.model.RoleModule;
import com.ra.model.RaUser;
public interface ModuleService {
	List<Module> selectAllList() ;

	int addModule(Module module) ;

	int updateModule(Module module) ;

	Module selectByKey(Integer sourceId) ;

	int deleteModuleByKey(List<?> list) ;

	List<Module> selectModuleByRoleid(List<?> ids) ;

	List<Module> selectModuleByUser(RaUser user) ;

	List<RoleModule> selectRoleModuleByRoleId(Long roleId) ;

	Map<Integer, RoleModule> selectRoleModuleByUser(RaUser user) ;
	
	List<Module> selectModuleList(Integer roleId);

	List<String> selectTestList();
}
