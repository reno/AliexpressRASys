package com.aliexpress.service	;

import java.util.List;

import com.aliexpress.bean.PageInfo;
import com.aliexpress.model.Function;
public interface FunctionService {
	List<Function> showAllList() ;

	List<Function> selectList(Function function, PageInfo pageVo) ;

	int selectTotalRecord(Function function) ;

	int addFunction(Function function) ;

	int updateFunction(Function function) ;

	Function selectByKey(Integer functionId) ;

	int deleteFunctionByKey(List<?> list) ;

}
