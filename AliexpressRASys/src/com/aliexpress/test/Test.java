package com.aliexpress.test;

import java.io.IOException;

import net.sf.json.JSONArray;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;

import com.aliexpress.constant.StaticConstants;
import com.aliexpress.cookies.CookiesParse.ChromeBrowser;

public class Test {
	public static void main(String[] args) throws IOException {
		if (null == StaticConstants.cookies) {
			ChromeBrowser chrome = new ChromeBrowser();
			StaticConstants.cookies = chrome.getCookiesFromFile("D:/java/files/cookies/Cookies");
		}
		Document doc = Jsoup.connect("https://www.dianxiaomi.com/smtCategory/attributeList.json?categoryId=200000347")//
				.timeout(1000 * 20)//
				.ignoreContentType(true)//
				.maxBodySize(0)// 不限长度
				.maxBodySize(0).header("User-Agent", StaticConstants.userAgent)//
				.cookies(StaticConstants.cookies)//
				.get();
		Element body = doc.body();
		JSONArray array = JSONArray.fromObject(body.text());
		System.out.println(array);
	}
}
