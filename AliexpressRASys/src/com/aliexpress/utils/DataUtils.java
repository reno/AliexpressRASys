package com.aliexpress.utils;

import java.math.BigDecimal;

public class DataUtils {
	public static Double mathRound(Double d, int n) {
		BigDecimal b = new BigDecimal(d);
		return b.setScale(n, BigDecimal.ROUND_FLOOR).doubleValue();
	}

	// 去除空格
	public static Object isNull(Object object) {
		return null == object ? " " : object;
	}

	// 去除空格
	public static String format(String object) {
		return null == object ? " " : object.replaceAll(",", "").replaceAll("\r\n", "").replaceAll("\n", "").replaceAll("\r", "");
	}
}
