package com.aliexpress.utils;

import com.qiniu.common.QiniuException;
import com.qiniu.common.Zone;
import com.qiniu.http.Response;
import com.qiniu.storage.Configuration;
import com.qiniu.storage.UploadManager;
import com.qiniu.util.Auth;

public class UploadUtils {
	static Zone z = Zone.zone0();
	static Configuration c = new Configuration(z);
	static UploadManager uploadManager = new UploadManager(c);

	public static String upload(byte[] bytes) throws Exception {
		String path = null;
		try {
			// 设置好账号的ACCESS_KEY和SECRET_KEY
			Auth auth = Auth.create("f2fLrBiTcP3Ee_4wcyYXeFIJsfry2FY4njCSknBA", "1MPU3jg325H1pgIDFBbdu4XCH3YvBYKnK3OPh5TX");
			// 调用put方法上传
			String name = UUIDUtil.get32UUID() + ".jpg";
			Response res = uploadManager.put(bytes, name, auth.uploadToken("wishimg"));
			path = "http://ovmzowfr3.bkt.clouddn.com/" + name;
			// 打印返回的信息
			System.out.println(res.bodyString());
		} catch (QiniuException e) {
			Response r = e.response;
			// 请求失败时打印的异常的信息
			e.printStackTrace();
			try {
				// 响应的文本信息
				System.out.println(r.bodyString());
			} catch (QiniuException e1) {
				e1.printStackTrace();
			}
			throw new Exception("文件上传失败！");
		}
		return path;
	}
}