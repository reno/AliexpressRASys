package com.aliexpress.utils;

import java.util.ArrayList;
import java.util.List;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import com.aliexpress.constant.StaticConstants;
import com.aliexpress.cookies.CookiesParse.ChromeBrowser;

public class ProductUtils {
	public static List<String> queryCid(String name, String cookiesPath) {
		try {
			if (null == StaticConstants.cookies) {
				ChromeBrowser chrome = new ChromeBrowser();
				StaticConstants.cookies = chrome.getCookiesFromFile(cookiesPath);
			}
			Document doc = Jsoup.connect("https://www.dianxiaomi.com/product/pageList.htm").timeout(1000 * 20)//
					.ignoreContentType(true)//
					.maxBodySize(0)// 不限长度
					.data("searchType", "2")//
					.data("searchValue", name)//
					.maxBodySize(0)// 不限长度
					.header("User-Agent", StaticConstants.userAgent)//
					.cookies(StaticConstants.cookies)//
					.post();
			// System.out.println(doc);
			Element goods = doc.getElementById("goodsContent");
			if (null != goods) {
				List<String> list = new ArrayList<String>();
				Elements els = goods.getElementsByClass("limingcentUrlpic");
				Elements as = null;
				if (null != els && els.size() > 0) {
					for (Element el : els) {
						as = el.getElementsByTag("a");
						if (null != as && as.size() > 0) {
							list.add(as.get(0).text());
						}
					}
				}
				return list;
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}

	public static void main(String[] args) {
		System.out.println(queryCid("wall sticker Dance as.. 0776 stickers manufacturers cartoon style living room bedroom, children's room wall decoration stickers for home deco vinyl", "D:/java/files/cookies/Cookies"));
	}
}
