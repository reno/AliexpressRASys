package com.aliexpress.bean;

import java.util.Map;

public class ParamBean {
	private Map<String, String> cookies;
	private String userAgent = "Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/60.0.3112.90 Safari/537.36";
	private String xsrf;
	private String regionUrl;
	public Boolean isLogin = false;

	public String getRegionUrl() {
		return regionUrl;
	}

	public void setRegionUrl(String regionUrl) {
		this.regionUrl = regionUrl;
	}

	public Map<String, String> getCookies() {
		return cookies;
	}

	public void setCookies(Map<String, String> cookies) {
		this.cookies = cookies;
	}

	public String getUserAgent() {
		return userAgent;
	}

	public void setUserAgent(String userAgent) {
		this.userAgent = userAgent;
	}

	public String getXsrf() {
		return xsrf;
	}

	public void setXsrf(String xsrf) {
		this.xsrf = xsrf;
	}

	public Boolean getIsLogin() {
		return isLogin;
	}

	public void setIsLogin(Boolean isLogin) {
		this.isLogin = isLogin;
	}

	@Override
	public String toString() {
		return "ParamBean [cookies=" + cookies + ", userAgent=" + userAgent + ", xsrf=" + xsrf + ", isLogin=" + isLogin + "]";
	}
}
