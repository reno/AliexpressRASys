package com.aliexpress.bean;

public class AttributeBean {
	private Long id;
	private Long categoryId;
	private Integer spec;
	private Integer visible;
	private Integer customizedName;
	private Integer customizedPic;
	private Integer keyAttribute;
	private Integer sku;
	private Integer arrtNameId;
	private String nameZh;
	private String nameEn;
	private String inputType;
	private String attributeShowTypeValue;
	private Integer required;
	private String units;
	private Integer moreAttribute;
	private Integer isDel;
	private Integer dxmOrderNum;
	private String createTime;
	private String updateTime;
	private String values;
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public Long getCategoryId() {
		return categoryId;
	}
	public void setCategoryId(Long categoryId) {
		this.categoryId = categoryId;
	}
	public Integer getSpec() {
		return spec;
	}
	public void setSpec(Integer spec) {
		this.spec = spec;
	}
	public Integer getVisible() {
		return visible;
	}
	public void setVisible(Integer visible) {
		this.visible = visible;
	}
	public Integer getCustomizedName() {
		return customizedName;
	}
	public void setCustomizedName(Integer customizedName) {
		this.customizedName = customizedName;
	}
	public Integer getCustomizedPic() {
		return customizedPic;
	}
	public void setCustomizedPic(Integer customizedPic) {
		this.customizedPic = customizedPic;
	}
	public Integer getKeyAttribute() {
		return keyAttribute;
	}
	public void setKeyAttribute(Integer keyAttribute) {
		this.keyAttribute = keyAttribute;
	}
	public Integer getSku() {
		return sku;
	}
	public void setSku(Integer sku) {
		this.sku = sku;
	}
	public Integer getArrtNameId() {
		return arrtNameId;
	}
	public void setArrtNameId(Integer arrtNameId) {
		this.arrtNameId = arrtNameId;
	}
	public String getNameZh() {
		return nameZh;
	}
	public void setNameZh(String nameZh) {
		this.nameZh = nameZh;
	}
	public String getNameEn() {
		return nameEn;
	}
	public void setNameEn(String nameEn) {
		this.nameEn = nameEn;
	}
	public String getInputType() {
		return inputType;
	}
	public void setInputType(String inputType) {
		this.inputType = inputType;
	}
	public String getAttributeShowTypeValue() {
		return attributeShowTypeValue;
	}
	public void setAttributeShowTypeValue(String attributeShowTypeValue) {
		this.attributeShowTypeValue = attributeShowTypeValue;
	}
	public Integer getRequired() {
		return required;
	}
	public void setRequired(Integer required) {
		this.required = required;
	}
	public String getUnits() {
		return units;
	}
	public void setUnits(String units) {
		this.units = units;
	}
	public Integer getMoreAttribute() {
		return moreAttribute;
	}
	public void setMoreAttribute(Integer moreAttribute) {
		this.moreAttribute = moreAttribute;
	}
	public Integer getIsDel() {
		return isDel;
	}
	public void setIsDel(Integer isDel) {
		this.isDel = isDel;
	}
	public Integer getDxmOrderNum() {
		return dxmOrderNum;
	}
	public void setDxmOrderNum(Integer dxmOrderNum) {
		this.dxmOrderNum = dxmOrderNum;
	}
	public String getCreateTime() {
		return createTime;
	}
	public void setCreateTime(String createTime) {
		this.createTime = createTime;
	}
	public String getUpdateTime() {
		return updateTime;
	}
	public void setUpdateTime(String updateTime) {
		this.updateTime = updateTime;
	}
	public String getValues() {
		return values;
	}
	public void setValues(String values) {
		this.values = values;
	}
}
