package com.aliexpress.bean;

public class GoodsDetailBean {
	private String addressOwner;
	private Double stockNum;
	private String orderNo;
	private String orderTime;

	public String getAddressOwner() {
		return addressOwner;
	}

	public void setAddressOwner(String addressOwner) {
		this.addressOwner = addressOwner;
	}

	public Double getStockNum() {
		return stockNum;
	}

	public void setStockNum(Double stockNum) {
		this.stockNum = stockNum;
	}

	public String getOrderNo() {
		return orderNo;
	}

	public void setOrderNo(String orderNo) {
		this.orderNo = orderNo;
	}

	public String getOrderTime() {
		return orderTime;
	}

	public void setOrderTime(String orderTime) {
		this.orderTime = orderTime;
	}

}
