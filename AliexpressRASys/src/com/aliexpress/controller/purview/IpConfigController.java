package com.aliexpress.controller.purview;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.aliexpress.bean.Result;
import com.aliexpress.constant.SysConstants;
import com.aliexpress.controller.common.BaseController;
import com.aliexpress.model.IpConfig;
import com.aliexpress.service.IpConfigService;

/**
 * @author JIAGUI
 * @email 1257896208@qq.com
 * @date 2017年4月25日 下午5:54:58
 * @description IP白名单管理
 */

@Controller(value = "IpConfigController")
@RequestMapping(value = "/ipconfig")
public class IpConfigController extends BaseController {
	@Autowired
	@Qualifier(value = "ipConfigServiceImpl")
	private IpConfigService configService;

	// 进入页面
	@RequestMapping(value = "/configform")
	public String ipConfigForm(Model model) {
		List<IpConfig> list = configService.selectAllList();
		if (null != list && list.size() > 0) {
			model.addAttribute("config", list.get(0));
		}
		return "purview/ipconfig/configform";
	}

	/**
	 * 增加
	 * 
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/save", method = RequestMethod.POST, produces = "application/json;charset=utf-8")
	public @ResponseBody Result save(IpConfig config, HttpServletRequest request) {
		Result result = new Result();
		int num = 0;
		try {
			if (config.getId() == null) {
				num = configService.add(config);
			} else {
				num = configService.update(config);
			}
			if (num > 0) {
				result.setCode(SysConstants.STATUS_TRUE);
				result.setMsg("保存成功!");
			} else {
				result.setCode(SysConstants.STATUS_FALSE);
				result.setMsg("保存失败!");
			}
		} catch (Exception e) {
			result.setCode(SysConstants.STATUS_ERROR);
			result.setMsg("系统错误!");
			result.setObject(config);
		}
		return result;
	}
}
