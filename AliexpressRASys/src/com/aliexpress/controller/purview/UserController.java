package com.aliexpress.controller.purview;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.aliexpress.constant.StaticConstants;
import com.aliexpress.constant.SysConstants;
import com.aliexpress.controller.common.BaseController;
import com.aliexpress.service.ModuleService;
import com.aliexpress.service.UserService;
import com.ra.login.LoginInfo;
import com.ra.login.LoginResult;
import com.ra.login.RaLoginUtils;
import com.ra.model.RaUser;

/**
 * @project House-Intro-Web
 * @package com.xinhai.houseintro.web.purview
 * @class UserController.java
 * @author jiagui E-mail:1257896208@qq.com
 * @date 2015年10月25日 下午5:47:27
 * @description 后台用户管理模块
 */
@Controller(value = "UserController")
@RequestMapping(value = "/user")
public class UserController extends BaseController {
	@Autowired
	@Qualifier(value = "userServiceImpl")
	private UserService userService;
	@Autowired
	@Qualifier(value = "moduleServiceImpl")
	private ModuleService moduleService;
	private static final Logger log = Logger.getLogger(UserController.class);
	@Value("${app.version}")
	private String version;

	// 进入页面
	@RequestMapping(value = "/userlist")
	public String userList() {
		log.info("进入用户页面...");
		return "purview/user/userlist";
	}

	@RequestMapping(value = "/userform")
	public String userform() {
		return "purview/user/userform";
	}

	@RequestMapping(value = "/updatepassword")
	public String updatepassword() {
		return "purview/user/updatepassword";
	}

	@RequestMapping(value = "/tologin")
	public String tologin(Model model) {
		return "purview/user/tologin";
	}

	@RequestMapping(value = "/login")
	public String login(HttpSession session, Model model) {
		session.setAttribute("version", version);
		return "purview/user/login";
	}

	@RequestMapping(value = "/index")
	public String index(Model model) {
		return "index";
	}

	@RequestMapping(value = "/showList", produces = "application/json;charset=utf-8")
	public @ResponseBody LoginResult showList(RaUser user, com.ra.login.PageInfo pageInfo) {
		log.info("查询User数据");
		LoginResult result = null;
		try {
			result = RaLoginUtils.getList(user, pageInfo);
			if (result.isSuccess()) {
				for (RaUser u : result.getUsers()) {
					u.setRoleIds(userService.selectRoleNamesByKey(u.getNid()));
				}
			}
			result.setObject(StaticConstants.roleMap);
		} catch (Exception e) {// 异常处理
			log.info(e.getMessage());
		}
		return result;
	}

	/**
	 * 增加
	 * 
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/addUser", method = RequestMethod.POST, produces = "application/json;charset=utf-8")
	public @ResponseBody LoginResult addUser(RaUser user, String roleIds) {
		LoginResult result = null;
		log.info("roleIds:" + roleIds);
		try {
			result = RaLoginUtils.add(user);
			if (result.isSuccess()) {
				userService.update(user, roleIds);
			}
		} catch (Exception e) {
			log.info(e.getMessage());
		}
		return result;
	}

	/**
	 * 资源修改页面
	 * 
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/showUpdate", produces = "application/json;charset=utf-8")
	public @ResponseBody LoginResult showUpdate(@RequestParam(required = false, value = "key") Long nid) {
		LoginResult result = null;
		try {
			result = RaLoginUtils.getUserById(nid);
			if (result.isSuccess()) {
				RaUser user = result.getUser();
				user.setRoleIds(userService.selectRoleIdsByKey(nid));
				result.setUser(user);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return result;
	}

	/**
	 * 批量删除
	 * 
	 * @param response
	 * @param userId
	 * @throws Exception
	 */
	@RequestMapping(value = "/deleteUser", produces = "application/json;charset=utf-8")
	public @ResponseBody LoginResult deleteUser(@RequestParam(required = false, value = "keys") String keys) {
		log.info("主键：" + keys);
		return RaLoginUtils.delete(keys);
	}

	/**
	 * 锁定、解锁用户
	 * 
	 * @param response
	 * @param userId
	 * @throws Exception
	 */
	@RequestMapping(value = "/updateUserLock", produces = "application/json;charset=utf-8")
	public @ResponseBody LoginResult updateUserLock(@RequestParam(required = false, value = "status") Integer used, @RequestParam(required = false, value = "key") Long nid) throws Exception {
		try {
			RaUser user = new RaUser();
			user.setNid(nid);
			user.setUsed(used);
			return RaLoginUtils.updateUserLock(user);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}

	/**
	 * 重置密码
	 * 
	 * @param response
	 * @param key
	 * @throws Exception
	 */
	@RequestMapping(value = "/resetPassword", produces = "application/json;charset=utf-8")
	public @ResponseBody LoginResult resetPassword(@RequestParam(required = false, value = "nid") Long nid) throws Exception {
		return RaLoginUtils.resetPassword(nid);
	}

	/**
	 * 重置密码
	 * 
	 * @param response
	 * @param key
	 * @throws Exception
	 */
	@RequestMapping(value = "/updatePassword", produces = "application/json;charset=utf-8")
	public @ResponseBody LoginResult updatePassword(RaUser user) throws Exception {
		return RaLoginUtils.updatePassword(user);
	}

	/**
	 * 登录验证
	 * 
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/dologin", produces = "application/json;charset=utf-8")
	public @ResponseBody LoginResult dologin(HttpSession session, HttpServletRequest request, RaUser user, Model model) throws Exception {
		LoginResult result = null;
		try {
			if (user != null) {
				LoginInfo info = new LoginInfo();
				info.setIp(this.gotIpAddr(request));
				info.setPlatform("RaaliexpressSys");
				result = RaLoginUtils.login(user, info);
				if (result.isSuccess()) {
					// 将登录信息放入session
					session.setAttribute(SysConstants.LOGIN_KEY, result.getUser());
					// 根据用户角色信息获取角色资源信息
					session.setAttribute(SysConstants.ROLE_RESOURCE_KEY, moduleService.selectRoleModuleByUser(result.getUser()));
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			result.setCode(SysConstants.STATUS_ERROR);
			result.setMsg("系统错误!");
		}
		return result;
	}

	@RequestMapping(value = "/exit")
	public String exit(HttpSession session, HttpServletRequest request, Model model) {
		try {
			session.setAttribute(SysConstants.LOGIN_KEY, null);
			session.setAttribute(SysConstants.ROLE_RESOURCE_KEY, null);
		} catch (Exception e) {
		}
		return "redirect:login";
	}
}
