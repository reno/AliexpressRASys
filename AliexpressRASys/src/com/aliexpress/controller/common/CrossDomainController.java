package com.aliexpress.controller.common;

import java.util.List;

import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.aliexpress.bean.ProductInfo;
import com.aliexpress.bean.Result;
import com.aliexpress.constant.SysConstants;
import com.aliexpress.datasource.DataSourceContextHolder;
import com.aliexpress.model.Menu;
import com.aliexpress.service.MenuService;
import com.aliexpress.utils.ProductUtils;

@Controller
@RequestMapping(value = "/cross")
public class CrossDomainController extends BaseController {
	private static final Logger log = Logger.getLogger(CrossDomainController.class);
	@Autowired
	private MenuService service;
	@Value("${cookies.path}")
	private String cookiesPath;

	/**
	 * 查询菜单列表
	 * 
	 * @return
	 */
	@RequestMapping(value = "/menuList", produces = "application/json;charset=utf-8")
	public @ResponseBody Result menuList(HttpServletResponse response) {
		Result result = new Result();
		response.addHeader("Access-Control-Allow-Origin", "*");
		try {
			List<Menu> list = service.selectAllList();// 查询符合条件的数据
			result.setCode(SysConstants.STATUS_TRUE);
			result.setList(list);
		} catch (Exception e) {// 异常处理
			log.info(e.getMessage());
			result.setCode(SysConstants.STATUS_ERROR);
			result.setMsg(e.getMessage());
		}
		return result;
	}

	/**
	 * 根据ID查询详细信息
	 * 
	 * @param menuId
	 * @return
	 */
	@RequestMapping(value = "/menuInfo/{menuId}", produces = "application/json;charset=utf-8")
	public @ResponseBody Result menuInfo(@PathVariable("menuId") String menuId, HttpServletResponse response) {
		response.addHeader("Access-Control-Allow-Origin", "*");
		Result result = new Result();
		try {
			Menu menu = service.selectById(menuId);
			result.setCode(SysConstants.STATUS_TRUE);
			result.setObject(menu);
		} catch (Exception e) {// 异常处理
			log.info(e.getMessage());
			result.setCode(SysConstants.STATUS_ERROR);
			result.setMsg(e.getMessage());
		}
		return result;
	}

	/**
	 * 根据ID查询详细信息
	 * 
	 * @param menuId
	 * @return
	 */
	@RequestMapping(value = "/productInfo/{name}", produces = "application/json;charset=utf-8")
	public @ResponseBody Result productInfo(@PathVariable("name") String name, HttpServletResponse response) {
		response.addHeader("Access-Control-Allow-Origin", "*");
		Result result = new Result();
		try {
			List<String> cids = ProductUtils.queryCid(name, cookiesPath);
			if (null != cids && cids.size() > 0) {
				List<ProductInfo> list = null;
				for (String cid : cids) {
					DataSourceContextHolder.setDbType(SysConstants.DATASOURCE_MYSQL_WISH);
					list = service.selectProductInfo(cid);
					if (null != list && list.size() > 0) {
						result.setCode(SysConstants.STATUS_TRUE);
						result.setList(list);
						break;
					}
				}
			} else {
				result.setCode(SysConstants.STATUS_FALSE);
				result.setMsg("查询失败！");
			}
			DataSourceContextHolder.setDbType(SysConstants.DATASOURCE_MYSQL);
		} catch (Exception e) {// 异常处理
			log.info(e.getMessage());
			result.setCode(SysConstants.STATUS_ERROR);
			result.setMsg(e.getMessage());
		}
		return result;
	}
}
