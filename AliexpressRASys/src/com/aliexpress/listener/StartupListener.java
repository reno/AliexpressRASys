package com.aliexpress.listener;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.ApplicationListener;
import org.springframework.context.event.ContextRefreshedEvent;
import org.springframework.stereotype.Service;

import com.aliexpress.dao.UserJobMapper;
import com.aliexpress.service.IpConfigService;

/**
 * @author JIAGUI
 * @email 1257896208@qq.com
 * @date 2017年4月25日 下午7:38:30
 * @description 启动完成执行
 */
@Service
public class StartupListener implements ApplicationListener<ContextRefreshedEvent> {
	@Autowired
	@Qualifier(value = "ipConfigServiceImpl")
	private IpConfigService configService;
	@Autowired
	private UserJobMapper jobMapper;
	@Value("${app.type}")
	private String type;

	@Override
	public void onApplicationEvent(ContextRefreshedEvent evt) {
		if (evt.getApplicationContext().getParent() == null) {

		}
	}
}