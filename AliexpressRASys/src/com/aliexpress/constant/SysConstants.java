package com.aliexpress.constant;

/**
 * SysConstants.java House-Intro-Common zhujiagui email:zhujiagui@zkingsoft.com
 * 2015年10月25日 下午3:28:44
 * 
 * @description 系统常量
 */
public interface SysConstants {
	/**
	 * 用户信息
	 */
	// 超级管理员
	String SUPERMAN = "zhujiagui";
	// 登录密码,123456
	String PASSWORD = "0a1592aabadd40fb467d81d849d7e342";
	// 登录会话key值
	String LOGIN_KEY = "loginInfo";// 后台登录用户信息
	String ROLE_RESOURCE_KEY = "roleResourceList";
	String MD5_KEY = "Fusion2015";// MD5加密密钥
	String AES_KEY = "Ck195324JiaGui17";
	/**
	 * 状态码
	 */
	String STATUS_TRUE = "true";// 成功
	String STATUS_FALSE = "false";// 失败
	String STATUS_ERROR = "error";// 错误
	String STATUS_INFO = "info";// 日志
	String STATUS_SYSTEM_ERROR = "system error";// 系统错误
	/** 正常 */
	Integer STATUS_NORMAL = 1;
	/** 异常 */
	Integer STATUS_ABNORMAL = 0;
	String DATASOURCE_MYSQL = "mysqlDataSource";
	String DATASOURCE_MYSQL_WISH = "wishDataSource";
	String DATASOURCE_SQLSERVER = "sqlServerDataSource";

	/**
	 * 分类类型常量
	 */
	enum CATE_TYPE {
		ALL, PARENT
	};
}
