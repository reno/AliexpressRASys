package com.aliexpress.constant;

import java.util.HashMap;
import java.util.Map;

public class StaticConstants {
	public static String savePath;
	public static String readPath;
	public static String separator;
	public static Map<String, String> roleMap = new HashMap<String, String>();
	public static final String userAgent = "Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/60.0.3112.90 Safari/537.36";
	public static Map<String, String> cookies = null;
}
