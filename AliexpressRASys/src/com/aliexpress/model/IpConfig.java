package com.aliexpress.model;

public class IpConfig {
    private Byte id;

    private String remark;

    private String ipContent;

    public Byte getId() {
        return id;
    }

    public void setId(Byte id) {
        this.id = id;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }

    public String getIpContent() {
        return ipContent;
    }

    public void setIpContent(String ipContent) {
        this.ipContent = ipContent;
    }
}